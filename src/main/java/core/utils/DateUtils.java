package core.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {
    public static final String HORA = "hhmmss";
    public static final String DATA = "ddMMyyyy";
    public static final String HORA_MOLDADA = "hh:mm:ss";
    public static final String DATA_MOLDADA = "dd/MM/yyyy";
    public static final String DATA_HORA_MOLDADA = "dd/MM/yyyy, hh:mm:ss";

    public static Date obterDataAtual() {
        return new Date(System.currentTimeMillis());
    }

    public static String formatarData(Date data, String format) {
        return new SimpleDateFormat(format).format(data);
    }

    public static Date alterarData(Date dataBase, int dias) {
        Calendar calendario = Calendar.getInstance();
        calendario.setTime(dataBase);
        calendario.add(Calendar.DATE, +dias);
        return calendario.getTime();
    }

    public static String obterDataEHoraAtualFormatada() {
        return formatarData(obterDataAtual(), "dd-MM-YY hh-mm");
    }
}

