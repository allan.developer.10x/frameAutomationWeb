package core;

import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static core.DriverFactory.getDriver;
import static core.utils.DateUtils.*;

public class BaseTest {
        protected static long initialTime;
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();

//    public void gravarVideo() throws ATUTestRecorderException {
//        recorder = new ATUTestRecorder("C:\\Users\\Allan.Caetano\\Desktop\\FrameWork\\target\\video" ,true);
//
//                recorder.start();
//    }

    protected void freeze(long seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace(System.err);
        }
    }
    @After
    public void addEvidencie() throws Exception {
        TakesScreenshot screen = (TakesScreenshot) getDriver();
        File arquivo = screen.getScreenshotAs(OutputType.FILE);
                 FileUtils.copyFile(arquivo, new File("target" + File.separator + "screenshot" +
                    File.separator + obterDataEHoraAtualFormatada() +  ".jpg"));
        if(Propiedades.FECHAR_BROWSER) {
            DriverFactory.killDriver();
        }
    }



}
