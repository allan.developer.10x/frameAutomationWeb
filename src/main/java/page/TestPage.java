package page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class TestPage {
    public TestPage(WebDriver driver ) {
        PageFactory.initElements(driver, this);
    }


    @FindBy(xpath = "//input[@name='q']" )
    private WebElement digitarPesquisa;

    public WebElement getDigitarPesquisa() {
        return digitarPesquisa;
    }

    @FindAll({@FindBy(xpath = "//div[.='Pular anúncios']")})
    private List<WebElement> pularAnuncio;

    public List<WebElement> getPularAnuncio() {
        return pularAnuncio;
    }


}
