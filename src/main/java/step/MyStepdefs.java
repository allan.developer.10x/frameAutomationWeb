package step;

import funcionalidade.TestFunc;
import core.BaseTest;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;

public class MyStepdefs  {
    TestFunc testFunc = new TestFunc();

    @Dado("acesso o goole")
    public void acessoOGoole() throws Throwable {
        testFunc.acessarWeb();
    }

    @E("digito no campo nome")
    public void digitoNoCampoNome() {
        testFunc.fazer();

    }

    @Quando("digito no campo sobreNome")
    public void digitoNoCampoSobreNome() {
    }

    @Entao("digito no campo idade")
    public void digitoNoCampoIdade() {
    }
}
